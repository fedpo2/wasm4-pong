
#[derive(Clone, Copy)]
pub struct Paleta {
    pub width: i32,
    pub height: i32,
    pub x: i32,
    pub y: i32,
}

impl Paleta {
    pub const fn new(b:bool) -> Paleta {
        Paleta{
            width: 4,
            height: 40,
            x: who_am_i(b),
            y: 50,
        }
    }

    pub const fn inside_bounds_up(&self) -> bool{
        if self.y > 0 {
            return true;
        }
        false
    }

    pub const fn inside_bounds_down(&self) -> bool{
        if self.y < 120 {
            return true;
        }
        false
    }
}

const fn who_am_i(b:bool) -> i32{
    if b {
        return 145;
    }
    5
    
}

pub struct Pelota {
    pub height: u32,
    pub width: u32,
    pub x: i32,
    pub y: i32,
    pub dy: i32,
    pub dx: i32, 
}

impl Pelota{
    pub const fn new() -> Pelota {
        Pelota {
            height: 4,
            width: 4, 
            x: 76,
            y: 76,
            dy: 2,
            dx: 1,
        }
    }

    pub fn movement(&mut self, punt:&mut [u8;2], paletas: [&Paleta;2]) -> [u8;2] {
        
        

        //anotacion de puntos (wip) + restableser ubicacion inicial
        if self.x < 0 {
            punt[0]+=1;
            self.reset_pelota();

        }
        if self.x > 160 {
            punt[1]+=1;
            self.reset_pelota();
        }

        //coliciones con las paletas
        if self.x <= paletas[0].x && self.y > paletas[0].y && self.y < paletas[0].y + paletas[0].height{
            self.dx = -self.dx+1;
            
        }
        
        if self.x >= paletas[1].x && self.y > paletas[1].y && self.y < paletas[1].y + paletas[1].height{
            self.dx = -self.dx-1;
        }
        
        //vertical boundaries
        if self.y > 160 || self.y < 0 {
            self.dy = -self.dy;
        }
        
        // movement on the x axis
        self.x += self.dx;
        
        //movement y axis
        self.y += self.dy;
        
        //valores de retorno (puntos)
        return *punt;  
    }
    
    fn reset_pelota(&mut self){
        self.y = 76;
        self.x = 76;
        
        //resetado de la velocidad
        if self.dx < 0 {
            self.dx=2;
            return;
        }

        self.dx = -2;

        //cambio de la velocidad vertical
        self.dy = -self.dy;
    }
}