mod wasm4;
mod paleta;

use paleta::{Paleta, Pelota};
use wasm4::*;

static mut PALETA1: Paleta = Paleta::new(false);
static mut PALETA2: Paleta = Paleta::new(true);
static mut PELOTA: Pelota = Pelota::new();
static mut PANTALLA: bool = false;
static mut PUNTAJES: [u8; 2] = [0,0];
// #[no_mangle]
// fn start(){

// }


#[no_mangle]
fn update(){
    unsafe{
        match PANTALLA {
            false => pantalla_menu(),
            true => pantalla_del_juego(),
        }
    }

}

fn pantalla_menu(){
    //texto del menu principal
    text("pong nefasto con\nonline!!1!!1\n\n\n\npd:si quiere jugar \ncon el multiplayer \nonline activelo ya\n\n\n\n\n\nx \npara ir al juego", 10,  10);
    
    //verificacion que uno de los dos jugadores apriete la x
    unsafe{
        let gamepad = *GAMEPAD1;
        if gamepad == BUTTON_1{
            PANTALLA = true;
        }
    }
}

fn pantalla_del_juego() {
    //literalmente una linea vertical
    vline(80, 0, 160);

    //handle del input y la logica
    handle_input();
    handle_logic();
    
    //muestra en pantalla de los objetos
    unsafe{
        draw_paleta(&PALETA1);
        draw_paleta(&PALETA2);
        draw_pelota(&PELOTA);
    }
}

fn handle_logic() {
    
    unsafe{

        //movimiento de la pelota + manejo del recuento de puntos
        PUNTAJES = Pelota::movement(&mut PELOTA, &mut PUNTAJES, [&PALETA1, &PALETA2]);
        
        // contador para reiniciar el juego futuramente 
        // text("", 10, 10);
        // text("", 130, 10);
    }
}

fn handle_input() {
    unsafe{
        if Paleta::inside_bounds_up(&PALETA1) {   
            if *GAMEPAD1 == BUTTON_UP {
                PALETA1.y -= 2;
            }
        }
        if Paleta::inside_bounds_down(&PALETA1){
            if *GAMEPAD1 == BUTTON_DOWN {
                PALETA1.y += 2;
            }
        }

        if Paleta::inside_bounds_up(&PALETA2) {   
            if *GAMEPAD2 == BUTTON_UP || *GAMEPAD1 == BUTTON_1 {
                PALETA2.y -= 2;
            }
        }
        if Paleta::inside_bounds_down(&PALETA2){
            if *GAMEPAD2 == BUTTON_DOWN || *GAMEPAD1 == BUTTON_2 {
                PALETA2.y += 2;
            }
        }
    } 
}

fn draw_paleta(pal:&Paleta){
    rect(pal.x, pal.y, pal.width.try_into().unwrap(), pal.height.try_into().unwrap());

}

fn draw_pelota(pal:&Pelota){
    rect(pal.x, pal.y, pal.width.try_into().unwrap(), pal.height.try_into().unwrap());

}


